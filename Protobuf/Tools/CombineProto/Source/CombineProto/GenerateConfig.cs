﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    public class GenerateConfig
    {
        public string packageName = "com.lh.demo.protocol";
        public string csCmdEnumName = "ECSCmd";
        public string scCmdEnumName = "ESCCmd";
        public string scCmdPreExtension = "SC_";
        public string csCmdPreExtension = "CS_";
        public string csharpMaxValue = "10000";
        public string luaMaxValue = "50000";
        public string sourceProtoRelatePath;
        public string csharpDistRelatePath;
        public string luaDistRelatePath;
        public string luaCSCmdReplatePath;
        public string luaSCCmdReplatePath;
    }
}
