﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    class EnumValue
    {
        public string name;
        public string msgName;
        public string value;
        public override string ToString()
        {
            return string.Format("\t{0} = {1};", name, value);
        }

        public string ToLuaCmd()
        {
            return string.Format("\t{0} = {1}", name, value);
        }

    }
}
