﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    public class CombineProto
    {
        private List<Message> m_cMessages = new List<Message>();
        private List<MessageEnum> m_cEnums = new List<MessageEnum>();
        private List<Message> m_luaMessages = new List<Message>();
        private List<MessageEnum> m_luaEnums = new List<MessageEnum>();
        private List<MessageEnum> m_allEnums = new List<MessageEnum>();
        private List<Message> m_allMessages = new List<Message>();
        private MessageEnum m_luaSCCmd;
        private MessageEnum m_luaCSCmd;
        private MessageEnum m_cSharpSCCmd;
        private MessageEnum m_cSharpCSCmd;
        private int csharpMaxValue;
        private int luaMaxValue;

        private GenerateConfig m_config;
        public CombineProto(GenerateConfig config)
        {
            m_config = config;
        }
        public void GenerateAll()
        {
            AynalizeProto();
            List<string> cmessagetypes = new List<string>();
            List<string> luamessagetypes = new List<string>();
            MessageEnum cMessage = null;
            MessageEnum luaMessage = null;
            int count = 0;
            csharpMaxValue = Convert.ToInt32(m_config.csharpMaxValue);
            luaMaxValue = Convert.ToInt32(m_config.luaMaxValue);
            for (int i = 0; i < m_allEnums.Count; i++)
            {
                if (m_allEnums[i].name.Equals(m_config.csCmdEnumName) || m_allEnums[i].name.Equals(m_config.scCmdEnumName))
                {
                    count++;
                    cMessage = new MessageEnum();
                    cMessage.name = m_allEnums[i].name;
                    luaMessage = new MessageEnum();
                    luaMessage.name = m_allEnums[i].name;
                    for (int j = 0; j < m_allEnums[i].dots.Count; j++)
                    {
                        EnumValue d = m_allEnums[i].dots[j];
                        int val = int.Parse(d.value);
                        if (val < csharpMaxValue)
                        {
                            cmessagetypes.Add(d.name.Replace(m_config.scCmdPreExtension, "").Replace(m_config.csCmdPreExtension, ""));
                            cMessage.dots.Add(d);
                        }
                        else if (val >= csharpMaxValue && val < luaMaxValue)
                        {
                            luamessagetypes.Add(d.name.Replace(m_config.scCmdPreExtension, "").Replace(m_config.csCmdPreExtension, ""));
                            luaMessage.dots.Add(d);
                        }
                    }

                    if (luaMessage.name.Equals(m_config.scCmdEnumName))
                    {
                        m_luaSCCmd = luaMessage;
                    }
                    else if (luaMessage.name.Equals(m_config.csCmdEnumName))
                    {
                        m_luaCSCmd = luaMessage;
                    }

                    if (cMessage.name.Equals(m_config.scCmdEnumName))
                    {
                        m_cSharpSCCmd = cMessage;
                    }
                    else if (cMessage.name.Equals(m_config.csCmdEnumName))
                    {
                        m_cSharpCSCmd = cMessage;
                    }
                }
                if (count >= 2)
                {
                    break;
                }
            }
            m_cSharpSCCmd.RemoveGreater(csharpMaxValue);
            m_cSharpCSCmd.RemoveGreater(csharpMaxValue);
            m_cEnums.Add(m_cSharpSCCmd);
            m_cEnums.Add(m_cSharpCSCmd);
            m_luaCSCmd.RemoveLower(csharpMaxValue);
            m_luaSCCmd.RemoveGreater(luaMaxValue);
            m_luaEnums.Add(m_luaCSCmd);
            m_luaEnums.Add(m_luaSCCmd);
            for (int i = 0; i < cmessagetypes.Count; i++)
            {
                AddCMessage(cmessagetypes[i]);
            }

            for (int i = 0; i < luamessagetypes.Count; i++)
            {
                AddLuaMessage(luamessagetypes[i]);
            }
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaDistRelatePath), ToLuaPbFile());
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.csharpDistRelatePath), ToCSDll());
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaCSCmdReplatePath), ToLuaCSCmdFile());
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaSCCmdReplatePath), ToLuaSCCmdFile());
            //Console.WriteLine(Path.Combine(System.Environment.CurrentDirectory, m_config.luaCompilePath));
            //Console.WriteLine(Path.Combine(System.Environment.CurrentDirectory, m_config.csCompilePath));
            //string err1 = RunBat(Path.Combine(System.Environment.CurrentDirectory, m_config.luaCompilePath), m_config.luaCompileArguments);
            //string err2 = RunBat(Path.Combine(System.Environment.CurrentDirectory, m_config.csCompilePath), m_config.csCompileArguments);

            //callback(err1 + err2);
        }
        public void GenerateLua(Action<string> callback)
        {
            AynalizeProto();
            List<string> luamessagetypes = new List<string>();
            MessageEnum luaMessage = null;
            int count = 0;
            for (int i = 0; i < m_allEnums.Count; i++)
            {
                if (m_allEnums[i].name.Equals(m_config.csCmdEnumName) || m_allEnums[i].name.Equals(m_config.scCmdEnumName))
                {
                    count++;
                    luaMessage = new MessageEnum();
                    luaMessage.name = m_allEnums[i].name;
                    for (int j = 0; j < m_allEnums[i].dots.Count; j++)
                    {
                        EnumValue d = m_allEnums[i].dots[j];
                        int val = int.Parse(d.value);
                        if (val >= csharpMaxValue && val < luaMaxValue)
                        {
                            luamessagetypes.Add(d.name.Replace(m_config.scCmdPreExtension, "").Replace(m_config.csCmdPreExtension, ""));
                            luaMessage.dots.Add(d);
                        }
                    }

                    if (luaMessage.name.Equals(m_config.scCmdEnumName))
                    {
                        m_luaSCCmd = luaMessage;
                    }
                    else if (luaMessage.name.Equals(m_config.csCmdEnumName))
                    {
                        m_luaCSCmd = luaMessage;
                    }
                }
                if (count >= 2)
                {
                    break;
                }
            }
            m_luaCSCmd.RemoveLower(csharpMaxValue);
            m_luaSCCmd.RemoveGreater(luaMaxValue);
            m_luaEnums.Add(m_luaCSCmd);
            m_luaEnums.Add(m_luaSCCmd);

            for (int i = 0; i < luamessagetypes.Count; i++)
            {
                AddLuaMessage(luamessagetypes[i]);
            }
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaDistRelatePath), ToLuaPbFile());
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaCSCmdReplatePath), ToLuaCSCmdFile());
            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.luaSCCmdReplatePath), ToLuaSCCmdFile());

            //string err1 = RunBat(Path.Combine(System.Environment.CurrentDirectory, m_config.luaCompilePath), m_config.luaCompileArguments);
            //callback(err1);
        }
        public void GenerateCSharp()
        {
            AynalizeProto();
            List<string> messagetypes = new List<string>();
            MessageEnum cMessage = null;
            int count = 0;
            for (int i = 0; i < m_allEnums.Count; i++)
            {
                if (m_allEnums[i].name.Equals(m_config.csCmdEnumName) || m_allEnums[i].name.Equals(m_config.scCmdEnumName))
                {
                    count++;
                    cMessage = new MessageEnum();
                    cMessage.name = m_allEnums[i].name;
                    for (int j = 0; j < m_allEnums[i].dots.Count; j++)
                    {
                        EnumValue d = m_allEnums[i].dots[j];
                        int val = int.Parse(d.value);
                        if (val < csharpMaxValue)
                        {
                            messagetypes.Add(d.name.Replace(m_config.scCmdPreExtension, "").Replace(m_config.csCmdPreExtension, ""));
                            cMessage.dots.Add(d);
                        }
                    }

                    if (cMessage.name.Equals(m_config.scCmdEnumName))
                    {
                        m_cSharpSCCmd = cMessage;
                    }
                    else if (cMessage.name.Equals(m_config.csCmdEnumName))
                    {
                        m_cSharpCSCmd = cMessage;
                    }
                }
                if (count >= 2)
                {
                    break;
                }
            }
            m_cSharpSCCmd.RemoveGreater(csharpMaxValue);
            m_cSharpCSCmd.RemoveGreater(csharpMaxValue);
            m_cEnums.Add(m_cSharpSCCmd);
            m_cEnums.Add(m_cSharpCSCmd);
            for (int i = 0; i < messagetypes.Count; i++)
            {
                AddCMessage(messagetypes[i]);
            }

            SaveFile(Path.Combine(System.Environment.CurrentDirectory, m_config.csharpDistRelatePath), ToCSDll());

            //string err2 = RunBat(Path.Combine(System.Environment.CurrentDirectory, m_config.csCompilePath), m_config.csCompileArguments);
            //callback(err2);
        }
        private string ToCSDll()
        {
            string ret = string.Empty;
            for (int i = 0; i < m_cEnums.Count; i++)
            {
                ret += "\n\n" + m_cEnums[i].ToString();
            }
            for (int i = 0; i < m_cMessages.Count; i++)
            {
                ret += "\n\n" + m_cMessages[i].ToString();
            }
            return string.Format("package {0};{1}", m_config.packageName, ret);
        }
        private string ToLuaPbFile()
        {
            string ret = string.Empty;
            for (int i = 0; i < m_luaEnums.Count; i++)
            {
                ret += "\n\n" + m_luaEnums[i].ToString();
            }
            for (int i = 0; i < m_luaMessages.Count; i++)
            {
                ret += "\n\n" + m_luaMessages[i].ToString();
            }
            return string.Format("package {0};{1}", m_config.packageName, ret);
        }
        private string ToLuaCSCmdFile()
        {
            string ret = "--【协议号文件】由脚本自动产生，避免手动修改内容\n\n--协议包名\n";
            ret += "local package = \"" + m_config.packageName + "\";\n";
            ret += "return {\n";
            ret += m_luaCSCmd.ToLuaCmd() + ",\n";
            ret +=
                "  parse = function(self, id)\n" +
                "    for k in pairs(self) do\n" +
                "       if self[k]~=nil and type(self[k])==\"table\" and  self[k].id == id then\n" +
                "         return k,self[k].value;\n" +
                "       end\n" +
                "    end\n" +
                "    return nil;\n" +
                " end\n";
            ret += "}";
            return ret;
        }
        private string ToLuaSCCmdFile()
        {
            string ret = "--【协议号文件】由脚本自动产生，避免手动修改内容\n\n--协议包名\n";
            ret += "local package = \"" + m_config.packageName + "\";\n";
            ret += "return {\n";
            ret += m_luaSCCmd.ToLuaCmd() + ",\n";
            ret +=
                "  parse = function(self, id)\n" +
                "    for k in pairs(self) do\n" +
                "       if self[k]~=nil and type(self[k])==\"table\" and  self[k].id == id then\n" +
                "         return k,self[k].value;\n" +
                "       end\n" +
                "    end\n" +
                "    return nil;\n" +
                " end\n";
            ret += "}";
            return ret;
        }
        private void AddCMessage(string name)
        {
            bool flag = false;
            for (int i = 0; i < m_allMessages.Count; i++)
            {
                if (m_allMessages[i].name.Equals(name))
                {
                    flag = true;
                    if (!m_cMessages.Contains(m_allMessages[i]))
                    {
                        m_cMessages.Add(m_allMessages[i]);
                        for (int j = 0; j < m_allMessages[i].dots.Count; j++)
                        {
                            MessageValue d = m_allMessages[i].dots[j];
                            if (!(d.type.Equals("int32") || d.type.Equals("int64") || d.type.Equals("string") || d.type.Equals("float")))
                            {
                                AddCMessage(d.type);
                            }
                        }
                    }
                    break;
                }
            }

            if (!flag)
            {
                for (int i = 0; i < m_allEnums.Count; i++)
                {
                    if (m_allEnums[i].name.Equals(name))
                    {
                        flag = true;
                        if (!m_cEnums.Contains(m_allEnums[i]))
                        {
                            m_cEnums.Add(m_allEnums[i]);
                        }
                        break;
                    }
                }
            }
        }
        private void AddLuaMessage(string name)
        {
            bool flag = false;
            for (int i = 0; i < m_allMessages.Count; i++)
            {
                if (m_allMessages[i].name.Equals(name))
                {
                    flag = true;
                    if (!m_luaMessages.Contains(m_allMessages[i]))
                    {
                        m_luaMessages.Add(m_allMessages[i]);
                        for (int j = 0; j < m_allMessages[i].dots.Count; j++)
                        {
                            MessageValue d = m_allMessages[i].dots[j];
                            if (!(d.type.Equals("int32") || d.type.Equals("int64") || d.type.Equals("string") || d.type.Equals("float")))
                            {
                                AddLuaMessage(d.type);
                            }
                        }
                    }
                    break;
                }
            }

            if (!flag)
            {
                for (int i = 0; i < m_allEnums.Count; i++)
                {
                    if (m_allEnums[i].name.Equals(name))
                    {
                        flag = true;
                        if (!m_luaEnums.Contains(m_allEnums[i]))
                        {
                            m_luaEnums.Add(m_allEnums[i]);
                        }
                        break;
                    }
                }
            }
        }
        private string RunBat(string batPath, string argu)
        {
            using (Process proc = new Process())
            {
                FileInfo file = new FileInfo(batPath);
                proc.StartInfo.Verb = "call";
                proc.StartInfo.FileName = batPath;
                //proc.StartInfo.Arguments = "< nul";
                proc.StartInfo.Arguments = string.IsNullOrEmpty(argu) ? "< null" : argu;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.WorkingDirectory = file.Directory.FullName;
                proc.EnableRaisingEvents = true;
                proc.StartInfo.CreateNoWindow = true;
                StringBuilder errorSb = new StringBuilder();
                //StringBuilder outputSb = new StringBuilder();
                proc.ErrorDataReceived += (sender, args) => errorSb.Append(args.Data);
                //proc.OutputDataReceived += (sender, args) => outputSb.AppendLine(args.Data);
                proc.Start();
                //proc.BeginOutputReadLine();
                proc.BeginErrorReadLine();
                proc.StandardInput.WriteLine("exit");
                try
                {
                    //proc.WaitForInputIdle();
                    proc.StandardInput.WriteLine();
                }
                catch (Exception)
                {
                    throw;
                }
                //proc.WaitForExit();
                proc.StandardOutput.ReadToEnd();
                if (errorSb.Length > 0)
                {
                    return errorSb.ToString();
                }
            }
            return string.Empty;
        }
        private void AynalizeProto()
        {
            string protoPath = Path.Combine(System.Environment.CurrentDirectory, m_config.sourceProtoRelatePath);//QuickUnity.DirectorySetting.ProtoExportPath();
            DirectoryInfo dir = new DirectoryInfo(protoPath);

            FileInfo[] files = dir.GetFiles("*.proto", SearchOption.AllDirectories);
            if (files.Length == 0)
            {
                //EditorUtility.DisplayDialog("协议", "Proto文件夹为空！!！!", "关闭");
                return;
            }

            List<string> textLines = new List<string>();
            for (int i = 0; i < files.Length; i++)
            {
                using (StreamReader r = files[i].OpenText())
                {
                    string text = r.ReadToEnd();
                    text = text.Replace("\r", "");
                    string[] ts = text.Split('\n');
                    for (int j = 0; j < ts.Length; j++)
                    {
                        if (!(ts[j].Contains("import") || ts[j].Contains("package " + m_config.packageName + ";") || ts[j].Equals("")))
                        {
                            if (ts[j].Contains("//"))
                            {
                                ts[j] = ts[j].Substring(0, ts[j].IndexOf("//"));
                            }
                            if (!string.IsNullOrEmpty(ts[j]))
                            {
                                textLines.Add(ts[j]);
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < textLines.Count;)
            {
                if (textLines[i].Contains("enum"))
                {
                    MessageEnum e = new MessageEnum();
                    string str = textLines[i];
                    string[] tempStr = str.Split(' ');
                    int tempStrCount = 0;
                    for (int j = 0; j < tempStr.Length; j++)
                    {
                        string temp = tempStr[j].Replace(" ", "").Replace("\t", "");
                        if (!string.IsNullOrEmpty(temp))
                        {
                            tempStrCount++;
                        }
                        if (tempStrCount == 2)
                        {
                            e.name = temp.Replace("{", "");
                            break;
                        }
                    }
                    bool multiLineFlag = false;
                    for (int j = i + 1; j < textLines.Count; j++)
                    {
                        if (textLines[j].Contains("}"))
                        {
                            i = j + 1;
                            break;
                        }
                        bool endFlag = false;
                        string content = textLines[j];
                        if (content.Contains("}"))
                        {
                            endFlag = true;
                            content = content.Replace("}", "");
                        }
                        if (content.Contains("//"))
                        {
                            content = content.Substring(0, content.IndexOf("//"));
                        }

                        if (content.Contains("/*"))
                        {
                            multiLineFlag = true;
                        }
                        if (content.Contains("*/"))
                        {
                            multiLineFlag = false;
                            continue;
                        }
                        if (multiLineFlag)
                        {
                            continue;
                        }
                        content = content.Replace("{", "");
                        if (string.IsNullOrEmpty(content.Replace(" ", "").Replace("\t", "")))
                        {
                            if (endFlag)
                            {
                                i = j + 1;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        EnumValue d = new EnumValue();
                        string[] tempStr2 = content.Split('=');
                        tempStrCount = 0;
                        for (int k = 0; k < tempStr2.Length; k++)
                        {
                            string temp = tempStr2[k].Replace(" ", "").Replace("\t", "");
                            if (!string.IsNullOrEmpty(temp))
                            {
                                tempStrCount++;
                                if (tempStrCount == 1)
                                {
                                    d.name = temp;
                                    d.msgName = temp.Replace(m_config.scCmdPreExtension, "").Replace(m_config.csCmdPreExtension, "");
                                }
                                else if (tempStrCount == 2)
                                {
                                    d.value = temp;
                                    break;
                                }
                            }
                        }
                        d.value = tempStr2[1].Replace(" ", "").Replace(";", "");
                        if (d.value.Contains("["))
                        {
                            d.value = d.value.Substring(0, d.value.IndexOf("["));
                        }
                        e.dots.Add(d);
                        if (endFlag)
                        {
                            i = j + 1;
                            break;
                        }
                    }
                    m_allEnums.Add(e);
                }
                else if (textLines[i].Contains("message"))
                {
                    Message m = new Message();
                    string str = textLines[i];
                    if (str.Contains("//"))
                    {
                        str = str.Substring(0, str.IndexOf("//"));
                    }

                    string[] tempStr = str.Split(' ');
                    int tempStrCount = 0;
                    for (int j = 0; j < tempStr.Length; j++)
                    {
                        string temp = tempStr[j].Replace(" ", "").Replace("\t", "");
                        if (!string.IsNullOrEmpty(temp))
                        {
                            tempStrCount++;
                        }
                        if (tempStrCount == 2)
                        {
                            m.name = temp.Replace("{", "");
                            break;
                        }
                    }

                    bool multiLineFlag = false;
                    for (int j = i + 1; j < textLines.Count; j++)
                    {
                        bool endFlag = false;
                        string content = textLines[j];
                        if (content.Contains("}"))
                        {
                            endFlag = true;
                            content = content.Replace("}", "");
                        }

                        if (content.Contains("//"))
                        {
                            content = content.Substring(0, content.IndexOf("//"));
                        }

                        if (content.Contains("/*"))
                        {
                            multiLineFlag = true;
                        }
                        if (content.Contains("*/"))
                        {
                            multiLineFlag = false;
                            continue;
                        }
                        if (multiLineFlag)
                        {
                            continue;
                        }
                        content = content.Replace("{", "");
                        if (string.IsNullOrEmpty(content.Replace(" ", "").Replace("\t", "")))
                        {
                            if (endFlag)
                            {
                                i = j + 1;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        MessageValue d = new MessageValue();
                        string[] tempStr2 = content.Split('=');
                        string[] tempStr3 = tempStr2[0].Split(' ');
                        tempStrCount = 0;
                        for (int k = 0; k < tempStr3.Length; k++)
                        {
                            string temp = tempStr3[k].Replace(" ", "");
                            if (!string.IsNullOrEmpty(temp))
                            {
                                tempStrCount++;
                                if (tempStrCount == 1)
                                {
                                    d.tag = temp;
                                }
                                else if (tempStrCount == 2)
                                {
                                    d.type = temp;
                                }
                                else if (tempStrCount == 3)
                                {
                                    d.name = temp;
                                    break;
                                }
                            }
                        }
                        if (tempStr2.Length < 2)
                        {
                            throw new Exception(string.Concat(tempStr2));
                        }
                        d.value = tempStr2[1].Replace(" ", "").Replace(";", "");
                        if (d.value.Contains("["))
                        {
                            d.value = d.value.Substring(0, d.value.IndexOf("["));
                        }
                        m.dots.Add(d);
                        if (endFlag)
                        {
                            i = j + 1;
                            break;
                        }
                    }
                    m_allMessages.Add(m);
                }
                else
                {
                    i++;
                }
            }
        }
        private void SaveFile(string path, string content)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(content);
                }
            }

        }
    }
}
