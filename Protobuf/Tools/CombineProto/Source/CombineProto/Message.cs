﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    class Message
    {
        public string name;
        public List<MessageValue> dots = new List<MessageValue>();

        public override string ToString()
        {
            string ret = string.Empty;
            for (int i = 0; i < dots.Count; i++)
            {
                ret += "\n" + dots[i].ToString();
            }
            return "message " + name + " {" + ret + "\n}";
        }
    }
}
