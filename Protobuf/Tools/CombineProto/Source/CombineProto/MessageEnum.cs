﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    class MessageEnum
    {
        public string name;
        public List<EnumValue> dots = new List<EnumValue>();

        public override string ToString()
        {
            string str = "enum " + name + " {";
            for (int i = 0; i < dots.Count; i++)
            {
                str += "\n" + dots[i].name + " = " + dots[i].value + ";";
            }
            str += "\n}";
            return str;
        }
        public void RemoveGreater(int value)
        {
            for (int i = 0; i < dots.Count; i++)
            {
                int par = int.Parse(dots[i].value);
                if (par > value)
                {
                    dots.RemoveAt(i);
                    i--;
                }
            }
        }
        public void RemoveLower(int value)
        {
            for (int i = 0; i < dots.Count; i++)
            {
                int par = int.Parse(dots[i].value);
                if (par < value)
                {
                    dots.RemoveAt(i);
                    i--;
                }
            }
        }
        public string ToLuaCmd()
        {
            string ret = "";
            for (int i = 0; i < dots.Count; i++)
            {
                var dot = dots[i];
                ret += "  [\"" + dot.name + "\"] = { id = " + dot.value + ", value = package..\"." + dot.msgName + "\"}";
                if (i < dots.Count - 1)
                {
                    ret += ",\n";
                }
            }
            return ret;
        }
    }
}
