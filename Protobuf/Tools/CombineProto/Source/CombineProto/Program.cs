﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    class Program
    {
        static int Main(string[] args)
        {
            GenerateConfig config = new GenerateConfig();
            for (int i = 0; i < args.Length; i++)
            {
                var split = args[i].Split('=');
                if (split.Length!=2)
                {
                    Console.Write("args is error:", args[i]);
                    continue;
                }
                string fieldName = split[0];
                string value = split[1];
                var fieldInfo=config.GetType().GetField(fieldName,System.Reflection.BindingFlags.Public| System.Reflection.BindingFlags.Instance);
                if (fieldInfo==null)
                {
                    Console.Write("dont has this param:" + fieldName);
                    return 1;
                }
                fieldInfo.SetValue(config, value);
            }
            CombineProto combineProto = new CombineProto(config);
            combineProto.GenerateAll();
            return 0;
        }
    }
}
