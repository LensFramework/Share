﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineProto
{
    class MessageValue
    {
        public string tag;
        public string type;
        public string name;
        public string value;

        public override string ToString()
        {
            return string.Format("\t{0} {1} {2} = {3};", tag, type, name, value);
        }
    }
}
