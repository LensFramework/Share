@setlocal enableextensions enabledelayedexpansion
@echo off

set file=config.ini
set splitString=""
set args=1
for /f "usebackq delims=" %%a in ("!file!") do (
	rem echo %%a
	if not "x!ln:~0,1!"=="x;" (
		if not !args! == 1 (
			set args=!args! %%a
		)
		if !args! == 1 (
			set args=%%a
		)
	)
)

start /WAIT CombineProto/Bin/CombineProto.exe !args!
cd CSharpProto
call generate.bat ../../Dist/Dll
cd ../LuaProto
call generate.bat ../../Dist/Lua
cd ..
pause

@endlocal