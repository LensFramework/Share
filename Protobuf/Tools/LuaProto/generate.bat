@echo off
set DIR=%~dp0
cd /d "%DIR%"
setlocal enabledelayedexpansion

for /r %%i in (*.proto) do (
      echo %%i
      echo %%~ni
      set pbname=%%i
      echo !pbname!
      set pbname=!pbname:~0,-5!bytes
      echo !pbname!
      protoc -I %DIR% --descriptor_set_out !pbname! %%i 
      move !pbname! %1
)
echo "finished"
