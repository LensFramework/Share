@setlocal enableextensions enabledelayedexpansion
@echo off
set file=config.ini
set findString=""
set contStr=""
set isContain=0
set splitString=""
set splitExt=""
set splitKey=""
set splitValue=""
set args=1
for /f "usebackq delims=" %%a in ("!file!") do (
	rem echo %%a
	if not "x!ln:~0,1!"=="x;" (
		REM echo %%a
		set findString=%%a
		set contStr="excelPath"
		call :strContains
		set splitString=%%a
		if !isContain! == 1 (
			call :Split
			set excelPath=!splitValue!
		)
		if !isContain! == 0 (
			set contStr="luaPath"
			call :strContains
			if !isContain! == 1 (
				call :Split
				set luaPath=!splitValue!
			)
			if !isContain! == 0 (
				set contStr="clientPath"
				call :strContains
				if !isContain! == 1 (
					call :Split
					set clientPath=!splitValue!
				)
				if !isContain! == 0 (
					set contStr="langFilePath"
					call :strContains
					if !isContain! == 1 (
						call :Split
						set langFilePath=!splitValue!
					)
					if !isContain! == 0 (
						if not !args! == 1 (
							set args=!args! -%%a
						)
						if !args! == 1 (
							set args=-%%a
						)
					)
				)
			)
		)
	)
)
XlsxToLua.exe !excelPath! !luaPath! !clientPath! !langFilePath! !args!
set errorLevel = %errorlevel%
if errorLevel == 0 (
	@echo 导出成功
	lua LuaDataOptimizer.lua
	@echo lua table 优化导出成功
) else (
	@echo 导出失败
)
pause

:strContains
echo !findString! | findStr !contStr! >nul
if !errorLevel! equ 0 (
	set isContain=1
) else (
	set isContain=0
)
goto:eof

:Split
for /f "delims=\=, tokens=1,*" %%i in ("!splitString!") do (
	set splitKey="%%i"
	set splitValue="%%j"
)
goto:eof

@endlocal

